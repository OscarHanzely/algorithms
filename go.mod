module algorithms

go 1.12

require (
	github.com/elazarl/goproxy v0.0.0-20190911111923-ecfe977594f1 // indirect
	github.com/mailru/easyjson v0.7.0 // indirect
	github.com/olivere/elastic/v7 v7.0.7
	github.com/parnurzeal/gorequest v0.2.16
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20191003171128-d98b1b443823 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
