package problems

// Problem given ans solved in this video: https://www.youtube.com/watch?v=EuPSibuIKIg
// 1) Imagine a x,y coordinate plane in cartesian system
// 2) Given is the list of points in this system point(x1,y1)...
// 3) Give the number of rectangles that can be formed on the plane by these coordinates
// 4) rectangles should be considered only the ones with parallel to the main axes

//f.e:
// .	.	.
// .	.	.
// contains 3 rectangles, left, right and perimeter one

// Solution count vertical lines that match the points y coordinates
// looking for points (x1,y1),(x1,y2),(x2,y1),(x2,y2)
// where x1 != x2 and y1 != y2

// Point struct holding point coordinates
type Point struct {
	X int
	Y int
}

// CountRectangles method to calculate the rectangles from given point list
func CountRectangles(data []*Point) (answer int) {
	type line struct{ y1, y2 int }
	verticalLines := make(map[line]int)

	for _, p1 := range data { // iterate over points
		for _, pAbove := range data { // looking for points that are directly above the selected one forming vertical line
			if p1.X == pAbove.X && p1.Y < pAbove.Y { // found a point that is directly above the first one at distance y2-y1

				// count the rectangles each rectangle is adding all previous plus one
				if v, ok := verticalLines[line{p1.Y, pAbove.Y}]; ok {
					answer += v
				}

				// store the found vertical line in hash map
				verticalLines[line{p1.Y, pAbove.Y}]++
			}
		}
	}

	return
}

