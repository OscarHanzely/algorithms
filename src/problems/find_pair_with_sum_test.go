package problems

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindPairSorted(t *testing.T) {
	sum := 8

	tests := []struct {
		in       []int
		sumFound bool
	}{
		{[]int{}, false},
		{[]int{10}, false},
		{[]int{1, 2, 3, 8, 9}, false},
		{[]int{1, 2, 4, 4, 6}, true},
		{[]int{0, 8, 9}, true},
		{[]int{-1, 2, 9}, true},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.in), func(t *testing.T) {
			assert.Equal(t, tt.sumFound, HasPairWithSumSorted(tt.in, sum))
		})
	}
}

func TestFindPair(t *testing.T) {
	sum := 8

	tests := []struct {
		in       []int
		sumFound bool
	}{
		{[]int{}, false},
		{[]int{-5}, false},
		{[]int{1, 8, 3, 8, 2}, false},
		{[]int{6, 4, 2, 1, 4, 6}, true},
		{[]int{0, 8, 9}, true},
		{[]int{10, 1, -1, 2, 9}, true},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.in), func(t *testing.T) {
			assert.Equal(t, tt.sumFound, HasPairWithSum(tt.in, sum))
		})
	}
}
