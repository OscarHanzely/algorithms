package problems

// https://www.interviewcake.com/question/java/stock-price
// Problem:
// Given a list of stock prices (integer) in chronological order, return the
// max profit from buying at earlier time and selling at later time.
//
// Example:
// Given:  []int{10, 7, 5, 8, 11, 9}
// Return: 6
//
// Solution:
// Use a greedy approach to keep a minimum and maximum price while iterating
// through the list.
//
// Cost:
// O(1) time, O(n) space.
// Similar solution can be found at: https://github.com/hoanhan101/algo/blob/master/interview_cake/apple_stocks_test.go

func getMaxProfit(stocks []int) int {
	// return 0 if the input is invalid
	if len(stocks) <= 2 {
		return 0
	}

	// set the first option to buy as first price in the list
	// set the potential profit as first available trade.
	// This assumes we would need to trade that day at least once if we would have option not to trade at all, profit declaration would be omitted
	buyingOption := stocks[0]
	profit := stocks[1] - stocks[0]

	for i := 1; i < len(stocks); i++ {
		currentPrice := stocks[i];
		potentialProfit := currentPrice - buyingOption

		if profit < (potentialProfit) {
			profit = potentialProfit
		}
		if buyingOption > currentPrice {
			buyingOption = currentPrice
		}
	}

	return profit
}
