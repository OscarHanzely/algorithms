package problems

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

// go test -bench=. -benchmem for Linux
// go test -bench . -benchmem for Windows
// Read more advanced benchmark commands at https://marcofranssen.nl/test-and-benchmark-your-code-in-go/

// Benchmark results:
//BenchmarkCanStopRecursive-4              5000000               366 ns/op				 0 B/op			 0 allocs/op
//BenchmarkCanStopIterative-4                   50          29500006 ns/op          539386 B/op       4847 allocs/op
//BenchmarkCanStopRecursiveWithCache-4    100000000               11.2 ns/op 			 0B/op			 0 allocs/op

// variable to store results of duplicateArrayTestFixtures to prevent any compiler optimizations
var record bool

func TestGenerateRunway(t *testing.T) {
	length := 100
	distribution := 20
	testCount := 100
	var nok int

	t.Run(strconv.Itoa(distribution)+"% distribution", func(t *testing.T) {
		for i := 0; i < testCount; i++ {
			var counter int
			for _, value := range generateRunway(length, distribution) {
				if !value {
					counter++
				}
			}

			tenPercent := length * 10 / 100
			if counter > (distribution+tenPercent) || counter < (distribution-tenPercent) {
				nok++
				//t.Errorf("generateRunway() method did not generate pseudo random slice with distribution %d%% +/- 10%%. Got %d out of %d", distribution, counter, length)
			}
		}

		if nok > (testCount * 10 / 100) {
			t.Errorf("generateRunway() method did not generate pseudo random slice with distribution %d%% +/- 10%%. Failed in %d cases out of %d", distribution, nok, testCount)
		}
		if nok > 0 {
			fmt.Printf("Distribution failed the range %d times\n", nok)
		}
	})
}

func TestCanStopRecursive(t *testing.T) {
	runway := generateRunway(1000, 20)
	startIndex := 10
	if runway[startIndex] { // just to make sure that we don't hit the first false by accident for test
		assert.True(t, CanStopRecursive(runway, 30, startIndex))
	}
}

func TestCanStopRecursiveWithCache(t *testing.T) {
	runway := generateRunway(1000, 20)
	startIndex := 10
	if runway[startIndex] { // just to make sure that we don't hit the first false by accident for test
		cache := make([]map[int]bool, len(runway))
		assert.True(t, CanStopRecursiveWithCache(runway, 30, startIndex, cache))
	}
}

func TestCanStopIterative(t *testing.T) {
	runway := generateRunway(1000, 20)
	startIndex := 10
	if runway[startIndex] { // just to make sure that we don't hit the first false by accident for test
		assert.True(t, CanStopIterative(runway, 30, startIndex))
	}
}

func BenchmarkCanStopRecursive(b *testing.B) {
	var r bool
	runway := generateRunway(1000, 20)
	b.ResetTimer() // to eliminate prep time spoil the results

	// run the benchmark function b.N times
	for n := 0; n < b.N; n++ {
		r = CanStopRecursive(runway, 30, 10)
	}

	record = r // this is here just to avoid any go compiler optimization
}

func BenchmarkCanStopIterative(b *testing.B) {
	var r bool
	runway := generateRunway(1000, 20)
	b.ResetTimer() // to eliminate prep time spoil the results

	// run the benchmark function b.N times
	for n := 0; n < b.N; n++ {
		r = CanStopIterative(runway, 30, 10)
	}

	record = r // this is here just to avoid any go compiler optimization
}

func BenchmarkCanStopRecursiveWithCache(b *testing.B) {
	var r bool
	runway := generateRunway(1000, 20)
	cache := make([]map[int]bool, len(runway))
	b.ResetTimer() // to eliminate prep time spoil the results

	// run the benchmark function b.N times
	for n := 0; n < b.N; n++ {
		r = CanStopRecursiveWithCache(runway, 30, 10, cache)
	}

	record = r // this is here just to avoid any go compiler optimization
}
