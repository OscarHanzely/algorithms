package problems

// https://leetcode.com/problems/add-two-numbers/solution/
//
//You have two non-empty linked lists representing two non-negative integers.
//The digits are stored in reverse order and each of their nodes contains a single digit.
//Add the two numbers and return it as a linked list.
//You may assume the two numbers do not contain any leading zero, except the number 0 itself.
//
//Use cases:
//   Input: (3 -> 5 -> 4) + (4 ->5 -> 3)
//   Output: 7 -> 0 -> 8
//   Explanation: 354 + 453 = 807
//

// Node struct
type Node struct {
	Val  int
	Next *Node
}

func addNumbers(l1, l2 *Node) *Node {
	result := &Node{} // initialize empty node, so we can return something
	curr := result
	var carry int
	for l1 != nil || l2 != nil {
		var x, y, sum int
		if l1 != nil {
			x = l1.Val
		}
		if l2 != nil {
			y = l2.Val
		}

		sum = carry + x + y
		carry = sum / 10

		curr.Next = &Node{Val: sum % 10}
		curr = curr.Next

		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
	}
	if carry > 0 {
		curr.Next = &Node{Val: carry}
	}

	return result.Next
}
