package problems

// Problem:
// Given a list of integers, return the highest product of three numbers.
//
// Example:
// Given:  []int{-10, -10, 1, 3, 2}
// Return: 300
//
// Solution(s): https://leetcode.com/articles/maximmum-product-of-three-numbers/#
// 1. Brute force
// 2. Sorting the list
// 3. Single scan of list
//
// Cost:
// 1. O(n^3) time, O(1) space
// 2. O(n log ⁡n) time, O(log n) space
// 3. O(n) time, O(1) space



// minmax returns min and max from a list of numbers.
func minmax(nums ...int) (int, int) {
	min, max := nums[0], nums[0]

	for _, num := range nums {
		if min > num {
			min = num
		}

		if max < num {
			max = num
		}
	}

	return min, max
}
