package problems

//Given an array of n integer elements which contains elements from 0 to n-1, with any numbers appearing any number of times.
//Find these repeating numbers using only constant memory space.
//For example, let n be 8 and array be {1, 2, 3, 1, 3, 6, 6, 3}, the answer should be 1, 3 and 6 (order is not determined).

//There are some interesting approaches which all fail on negative numbers or numbers bigger than n here
//https://www.geeksforgeeks.org/duplicates-in-an-array-in-on-time-and-by-using-o1-extra-space-set-3/

func findDuplicatesNestedLoops(input []int) []int {
	var res []int
	var duplicateIndices = make(map[int]int)

	for i := 0; i < len(input)-1; i++ {
		if duplicateIndices[i] != 1 { // int(1) used as mark value, skip checking if it is already a duplicate
			for j := len(input) - 1; j > i; j-- {
				if input[j] == input[i] {
					duplicateIndices[i] = 1
					if duplicateIndices[j] != 1 {
						duplicateIndices[j] = 1
						res = append(res, input[j])
					}
					break
				}
			}
		}
	}

	return res
}

func findDuplicatesTwoLoops(input []int) []int {
	var res []int
	var duplicateCounter = make(map[int]int)

	for i := 0; i < len(input); i++ {
		duplicateCounter[input[i]]++
	}

	// this is not order safe, because Go may randomize the order of keys in map !!!
	for i, val := range duplicateCounter {
		if val > 1 {
			res = append(res, i)
		}
	}

	return res
}

// some of the PHP solutions
// http://phpfiddle.org/main/code/kgrk-d4b0
// https://codepen.io/anon/pen/LwjZZP?editors=0012
// https://sandbox.onlinephpfunctions.com/code/eb223dffa6ff67ccca410b76e7b9911ba91152c1
