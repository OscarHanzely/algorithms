package problems

import "sort"

// https://www.interviewcake.com/question/java/merging-ranges
// Problem:
// Given a list of unsorted, independent meetings, returns a list of a merged
// one.
//
// Example:
// Given:  []meeting{{1, 2}, {2, 3}, {4, 5}}
// Return: []meeting{{1, 3}, {4, 5}}
//
// Solution:
// Sort the list in ascending order, iterate through the list and check if the
// last merged meeting end time is greater or equal then the current one start
// time, merge them using the later end time.
//
// Cost:
// O(nlogn) time, O(n) space.
//
// Solution from: https://github.com/hoanhan101/algo/blob/master/interview_cake/merge_meetings_test.go

// meeting has a start and end time.
type meeting struct {
	start int
	end   int
}

func mergeMeetings(meetings []meeting) []meeting {
	// sort the meetings in ascending order.
	sort.Slice(meetings, func(i, j int) bool {
		return meetings[i].start < meetings[j].start
	})

	result := []meeting{}
	for i := range meetings {
		// push the first meeting to the list so we can have a start.
		if i == 0 {
			result = append(result, meetings[i])
			continue
		}

		// if the last merged meeting's end time is greater or equal than the current
		// one's start time, merge them by using the later ending time.
		if result[len(result)-1].end >= meetings[i].start {
			if meetings[i].end >= result[len(result)-1].end {
				result[len(result)-1].end = meetings[i].end
			} else {
				result[len(result)-1].end = result[len(result)-1].end
			}
		} else {
			result = append(result, meetings[i])
		}
	}

	return result
}


