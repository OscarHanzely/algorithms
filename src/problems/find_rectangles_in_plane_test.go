package problems

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// Test points mapped https://imgur.com/a/XS3C0v0

func TestCountRectangles(t *testing.T) {
	points := []*Point{
		{ // A
			X: 5,
			Y: 2,
		},
		{ // B
			X: 5,
			Y: 5,
		},
		{ // C
			X: 3,
			Y: 2,
		},
		{ // D
			X: 3,
			Y: 5,
		},
		{ // E
			X: 10,
			Y: 2,
		},
		{ // F
			X: 10,
			Y: 5,
		},
		{ // G
			X: 6,
			Y: 4,
		},
		{ // H
			X: -5,
			Y: -3,
		},
		{ // I
			X: -5,
			Y: 2,
		},
		{ // J
			X: 6,
			Y: 1,
		},
	}

	// rectangles:
	// [C,D,B,A], [A,B,F,E], [C,D,F,E]
	expectedRectangles := 3

	t.Run("expected rectangles: 3", func(t *testing.T) {
		assert.Equal(t, expectedRectangles, CountRectangles(points))
	})
}
