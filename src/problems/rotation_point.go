package problems

// https://www.interviewcake.com/question/java/find-rotation-point
// Problem:
// Consider a list of alphabetically sorted words in ascending direction. The array has been rotated (clockwise) k number of times.
// Given such an array, find the value of k / find the index of rotation point.
//
// Example:
// Given:
//        []string{
// 	       "ptolemaic",
// 	       "retrograde",
// 	       "supplant",
// 	       "undulate",
// 	       "xenoepist",
// 	       "asymptote", // <-- rotates here!
// 	       "babka",
// 	       "banoffee",
// 	       "engender",
// 	       "karpatka",
// 	       "othellolagkage",
//        },
// Return: 5
//
// Solution(s):
// 1. Use a binary search approach to search for word. The rotation point is converged in the middle.
// 2. Linear search can do single pass of words and stop when the spike happens
//
// Cost:
// O(logn) time, O(1) space.

func findRotationPointBinarySearch(words []string) int {
	firstWord := words[0]
	minIndex := 0
	maxIndex := len(words) - 1

	for minIndex < maxIndex {
		// instead of (minIndex + maxIndex) / 2 use this principle from standard sort.Search() golang library
		// https://flaviocopes.com/golang-algorithms-binary-search/
		// https://github.com/golang/go/blob/master/src/sort/search.go
		middleIndex := minIndex + (maxIndex - minIndex) / 2 // avoid overflow when computing h

		// if the middle word comes after first one proceed search in right side of array
		// otherwise proceed in left side of array
		if words[middleIndex] > firstWord {
			minIndex = middleIndex
		} else {
			maxIndex = middleIndex
		}

		// minIndex and maxIndex have converged at rotation point
		if minIndex+1 == maxIndex {
			return maxIndex
		}
	}

	return 0
}

func findRotationPointLinearSearch(words []string) int {
	referenceWord := words[0]

	for i := 0; i < len(words); i++ {
		// traverse through the array and once you find a word that should come before the previous one return
		if referenceWord <= words[i] {
			referenceWord = words[i];
		} else {
			return i
		}
	}

	return 0
}
