package problems

import (
	"reflect"
	"sort"
	"testing"
)

var duplicateArrayTestFixtures = []struct {
	name   string
	input  []int
	output []int
}{
	{
		name:   "[1, 5, 7, 5, 4]",
		input:  []int{1, 5, 7, 5, 4},
		output: []int{5},
	},
	{
		name:   "[4, 2, 4, 2, 3]",
		input:  []int{4, 2, 4, 2, 3, -5, -5},
		output: []int{4, 2, -5},
	},
	{
		name:   "[1, 2, 3, 1, 3, 6, 6, 3]",
		input:  []int{1, 2, 3, 1, 3, 6, 6, 3},
		output: []int{1, 3, 6},
	},
	{
		name:   "[1, 1, 1]",
		input:  []int{1, 1, 1},
		output: []int{1},
	},
	{
		name:   "[7]",
		input:  []int{7},
		output: []int(nil),
	},
	{
		name:   "[]",
		input:  []int{},
		output: []int(nil),
	},
}

func Test_findDuplicatesNestedLoops(t *testing.T) {
	for _, tt := range duplicateArrayTestFixtures {
		t.Run(tt.name, func(t *testing.T) {
			if got := findDuplicatesNestedLoops(tt.input); !reflect.DeepEqual(got, tt.output) {
				t.Errorf("findDuplicatesNestedLoops() = %v, expected output %v", got, tt.output)
			}
		})
	}
}

func Test_findDuplicatesTwoLoops(t *testing.T) {
	for _, tt := range duplicateArrayTestFixtures {
		t.Run(tt.name, func(t *testing.T) {
			got := findDuplicatesTwoLoops(tt.input)
			sort.Ints(got) // we ned to sort in order to use reflect, because go maps don't guarantee order
			sort.Ints(tt.output)
			if !reflect.DeepEqual(got, tt.output) {
				t.Errorf("findDuplicatesTwoLoops() = %v, expected output %v", got, tt.output)
			}
		})
	}
}

// TODO write benchmarks
