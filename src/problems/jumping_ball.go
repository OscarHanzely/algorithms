package problems

import (
	"math/rand"
	"time"
)

// Example dynamic programming problem that demonstrates the type of interview coding exercises
// Source: https://www.freecodecamp.org/news/follow-these-steps-to-solve-any-dynamic-programming-interview-problem-cc98e508cd0e/
//
// Problem statement: In this problem, we’re on a crazy jumping ball, trying to stop, while avoiding spikes along the way.
// Problem rules:
// 		1) You’re given a flat runway with a bunch of spikes in it. The runway is represented by a boolean array which
// 		indicates if a particular (discrete) spot is clear of spikes. It is True for clear and False for not clear.
//		2) You’re given a starting speed S. S is a non-negative integer at any given point, and it indicates how much
//		you will move forward with the next jump.
//		3) Every time you land on a spot, you can adjust your speed by up to 1 unit before the next jump.
//		4) You want to safely stop anywhere along the runway (does not need to be at the end of the array). You stop
//		when your speed becomes 0. However, if you land on a spike at any point, your crazy bouncing ball bursts and it’s game over.
//		5) The output of your function should be a boolean indicating whether we can safely stop anywhere along the runway.

// CanStopRecursive recursive way of solving the problem
func CanStopRecursive(runway []bool, initSpeed int, startIndex int) bool {
	// base cases that are causing us to fail
	// start index is invalid || init speed is invalid || runway has spike, value == false
	if startIndex < 0 || startIndex >= len(runway) || initSpeed < 0 || !runway[startIndex] {
		return false
	}

	// base case that succeeds ~ stopping condition
	if initSpeed == 0 {
		return true
	}

	var speedOption = [3]int{initSpeed - 1, initSpeed, initSpeed + 1}

	// try all possible paths
	for _, adjustSpeed := range speedOption {
		if CanStopRecursive(runway, adjustSpeed, startIndex+adjustSpeed) {
			return true
		}
	}

	return false
}

// CanStopIterative iterative way of solving the problem
func CanStopIterative(runway []bool, initSpeed int, startIndex int) bool {
	// maximum speed cannot be more than length of the entire runway
	maxSpeed := len(runway)

	// base cases that are causing us to fail
	// start index is invalid || init speed is invalid || runway has spike, value == false
	if startIndex < 0 || startIndex >= len(runway) || initSpeed < 0 || initSpeed > maxSpeed || !runway[startIndex] {
		return false
	}

	// safe position i ~ set of speeds we can stop from position i
	// initialize to size of runway so we can access random index
	var safe = make([][]int, len(runway))

	// base cases for success, we can stop when and position is not spike and speed is 0
	for position, value := range runway {
		if value {
			safe[position] = []int{0}
		}
	}

	// outer loop to go over the runway positions from last one to first one
	for index := len(runway) - 1; index >= 0; index-- {
		// skip positions which contains spikes
		if !runway[index] {
			continue
		}

		// for each positions go over each possible speed
		for speed := 1; speed <= maxSpeed+1; speed++ {
			var speedOption = [3]int{speed - 1, speed, speed + 1}
			// try all possible paths
			for _, adjustedSpeed := range speedOption {
				// if we have ability to stop from the field at adjusted speed we add the current speed to the position option
				estPosition := index+adjustedSpeed
				if estPosition < len(safe) && safe[estPosition] != nil && valueInSlice(adjustedSpeed, safe[estPosition]) {
					safe[index] = append(safe[index], speed)
					break
				}
			}
		}
	}

	for _, j := range safe[startIndex] {
		if j == initSpeed {
			return true
		}
	}

	return false
}

// CanStopRecursiveWithCache version with memoization
// pkg: algorithms/problems/jumpingball
//BenchmarkCanStopRecursive-4             300000000              334 ns/op
//BenchmarkCanStopRecursiveWithCache-4    100000000               11.2 ns/op
func CanStopRecursiveWithCache(runway []bool, initSpeed int, startIndex int, cache []map[int]bool) bool {
	// only first time initialization
	if cache == nil {
		cache = make([]map[int]bool, len(runway))
	}

	// first check of the result exists in cache
	if startIndex < len(cache) {
		if val, ok := cache[startIndex][initSpeed]; ok {
			return val
		}
	}

	// base cases that are causing us to fail
	// start index is invalid || init speed is invalid || runway has spike, value == false
	if startIndex < 0 || startIndex >= len(runway) || initSpeed < 0 || !runway[startIndex] {
		return false
	}

	// base case that succeeds ~ stopping condition
	if initSpeed == 0 {
		insertIntoCache(cache, startIndex, initSpeed, true)
		return true
	}

	var speedOption = [3]int{initSpeed - 1, initSpeed, initSpeed + 1}

	// try all possible paths
	for _, adjustSpeed := range speedOption {
		if CanStopRecursiveWithCache(runway, adjustSpeed, startIndex+adjustSpeed, cache) {
			insertIntoCache(cache, startIndex, initSpeed, true)
			return true
		}
	}

	insertIntoCache(cache, startIndex, initSpeed, false)
	return false
}

func insertIntoCache(cache []map[int]bool, startIndex int, initSpeed int, canStop bool) {
	if startIndex < len(cache) {
		if cache[startIndex] == nil {
			cache[startIndex] = make(map[int]bool)
		}
		cache[startIndex][initSpeed] = canStop
	}
}

func valueInSlice(needle int, haystack []int) bool {
	for _, val := range haystack {
		if val == needle {
			return true
		}
	}
	return false
}

// generateRunway generate the boolean runway with spikes distribution in percentage
func generateRunway(length int, distribution int) []bool {
	if distribution < 1 {
		panic("Invalid distribution")
	}

	if distribution > 100 {
		distribution = 100
	}

	rand.Seed(time.Now().UnixNano())
	runway := make([]bool, length)
	for i := 0; i < length; i++ {
		runway[i] = (rand.Intn(100) + 1) < (100 - distribution)
	}
	return runway
}
