package problems

// This one comes from the Google example interview in this video
// https://www.youtube.com/watch?v=XKu_SEDAykw
// Problem:
// 1) Let's have a collection of integers (positive and negative) and a number in mind, find if there is a pair in collection
// which sum would give us the number we are looking for.
// Assume a collection of sorted numbers. Numbers can repeat and have any integer value.
// 2) How would solution change if collection would not be guaranteed to be sorted.
// Solution should be found in O(n) time

// HasPairWithSumSorted start with the pair and keep comparing and moving indices until I either find it or cors the indices
func HasPairWithSumSorted(data []int, sum int) bool {
	low := 0
	high := len(data) - 1

	for low < high {
		candidate := data[low] + data[high]
		if candidate == sum {
			return true
		}
		if candidate < sum {
			low++
		}
		if candidate > sum {
			high--
		}
	}

	return false
}

// HasPairWithSum keep the complement for the current value in hash table with constant time lookup
func HasPairWithSum(data []int, sum int) bool {
	complements := make(map[int]int)

	for _, v := range data {
		if _, ok := complements[v]; ok {
			return true
		}
		complements[sum-v] = v
	}

	return false
}
