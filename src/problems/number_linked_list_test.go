package problems

import (
	"reflect"
	"testing"
)

func Test_AddLinkedListNumbers(t *testing.T) {
	l1 := &Node{
		Val: 3,
		Next: &Node{
			Val: 5,
			Next: &Node{
				Val:  4,
				Next: nil,
			},
		},
	}
	l2 := &Node{
		Val: 4,
		Next: &Node{
			Val: 5,
			Next: &Node{
				Val:  3,
				Next: nil,
			},
		},
	}

	result := fillList(addNumbers(l1, l2))
	expected := []int{7, 0, 8}
	reflect.DeepEqual(expected, result)

	l1 = nil
	l2 = &Node{
		Val: 4,
		Next: &Node{
			Val: 5,
			Next: &Node{
				Val:  3,
				Next: nil,
			},
		},
	}

	result = fillList(addNumbers(l1, l2))
	expected = []int{4, 5, 3}
	reflect.DeepEqual(expected, result)

	l1 = &Node{
		Val: 3,
		Next: &Node{
			Val: 5,
			Next: &Node{
				Val:  4,
				Next: nil,
			},
		},
	}
	l2 = nil

	result = fillList(addNumbers(l1, l2))
	expected = []int{3, 5, 4}
	reflect.DeepEqual(expected, result)

}

func fillList(node *Node) []int {
	var list []int
	for node.Next != nil {
		list = append(list, node.Val)
		node = node.Next
	}
	list = append(list, node.Val)

	return list
}
